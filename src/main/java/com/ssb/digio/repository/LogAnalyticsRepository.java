package com.ssb.digio.repository;

import com.ssb.digio.entity.LogEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LogAnalyticsRepository extends JpaRepository<LogEntry, Long> {

    @Query("SELECT DISTINCT ipAddress FROM LogEntry ")
    List<String> findDistinctIpAddresses();

}
