package com.ssb.digio.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApplicationException extends RuntimeException {

    public ApplicationException(final String message) {
        super(message);
        log.error("APPLICATION EXCEPTION - {}", message);
    }

    public ApplicationException(final String message, final Throwable throwable) {
        super(message, throwable);
        log.error("APPLICATION EXCEPTION - {}", message, throwable);
    }
}
