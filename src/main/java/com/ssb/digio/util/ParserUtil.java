package com.ssb.digio.util;

import com.ssb.digio.entity.LogEntry;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ParserUtil {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/LLL/yyyy:HH:mm:ss Z");
    private static final Pattern LOG_EXTRACT_PATTERN = Pattern.compile("^(?<ip>[\\d.]+)\\s.*\\[(?<date>.*)]\\s\"((\\w+?)\\s(?<uri>.+?)?\\s)");

    public static LogEntry parseLog(final String line) {
        LogEntry logEntry = null;

        final Matcher matcher = LOG_EXTRACT_PATTERN.matcher(line);
        if (matcher.lookingAt()) {
            final String ip = matcher.group("ip");
            final ZonedDateTime accessDate = Optional.ofNullable(matcher.group("date"))
                    .map(e -> ZonedDateTime.parse(e, DATE_FORMAT)).orElse(null);
            final String uri = matcher.group("uri");

            logEntry = new LogEntry(ip, accessDate, uri);
        }

        return logEntry;
    }
}
