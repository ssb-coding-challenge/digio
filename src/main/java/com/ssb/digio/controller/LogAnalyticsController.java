package com.ssb.digio.controller;

import com.ssb.digio.service.LogAnalyticsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "analytics", description = "Analytics service")
@RestController
public class LogAnalyticsController {

    private final LogAnalyticsService logAnalyticsService;

    @Autowired
    public LogAnalyticsController(final LogAnalyticsService logAnalyticsService) {
        this.logAnalyticsService = logAnalyticsService;
    }

    @Operation(summary = "Get unique IP addresses")
    @GetMapping("/unique-ip")
    public List<String> getUniqueIpAddress() {
        return logAnalyticsService.getUniqueIpAddresses();
    }

    @Operation(summary = "Get top 3 most visited URLs")
    @GetMapping("/most-visited-urls")
    public List<String> getMostVisitedUrls() {
        // This endpoint can be enhanced to accept request parameter to configure top x most visited urls.
        return logAnalyticsService.getMostVisitedUrls();
    }

    @Operation(summary = "Get top 3 most active IPs")
    @GetMapping("/most-active-ips")
    public List<String> getMostActiveIPs() {
        // This endpoint can be enhanced to accept request parameter to configure top x most active IPs.
        return logAnalyticsService.getMostActiveIPs();
    }
}
