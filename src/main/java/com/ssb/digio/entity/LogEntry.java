package com.ssb.digio.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Entity
@Table(name = "log_entry", indexes = @Index(name = "idx_ip", columnList = "ip_address"))
public class LogEntry {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "access_date")
    private ZonedDateTime accessDate;

    @Column(name = "uri")
    private String uri;

    public LogEntry(final String ipAddress, final ZonedDateTime accessDate, final String uri) {
        this.ipAddress = ipAddress;
        this.accessDate = accessDate;
        this.uri = uri;
    }
}
