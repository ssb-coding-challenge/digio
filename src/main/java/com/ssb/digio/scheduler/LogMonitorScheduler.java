package com.ssb.digio.scheduler;

import com.ssb.digio.exception.ApplicationException;
import com.ssb.digio.service.LogMonitorListenerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;

@Slf4j
@Component
public class LogMonitorScheduler {

    @Value("${scheduler.enabled:true}")
    private boolean schedulerEnabled;
    @Value("${log.file-path}")
    private Resource accessLog;

    private long last = 0;
    private long position = 0;
    private final LogMonitorListenerService listenerService;

    @Autowired
    public LogMonitorScheduler(final LogMonitorListenerService listenerService) {
        this.listenerService = listenerService;
    }

    @Scheduled(initialDelayString = "${scheduler.initial-delay:20000}", fixedDelayString = "${scheduler.fixed-delay:30000}")
    public void readFile() {
        if (!schedulerEnabled) {
            return;
        }
        final File file;
        try {
            file = accessLog.getFile();
            final long length = file.length();
            if (!file.exists()) {
                log.error("File not found. Check the file path and prefix 'file:'");
                throw new FileNotFoundException();
            }
            final boolean newer = FileUtils.isFileNewer(file, last);

            if (newer && length > position) {
                log.info("File updated. Extracting data.");
                final BufferedReader br = new BufferedReader(new FileReader(file));
                br.skip(position);
                String line;
                while ((line = br.readLine()) != null) {
                    if (StringUtils.hasText(line)) {
                        listenerService.processRecord(line);
                    }
                }
                position = length;
                last = FileUtils.lastModified(file);
            }
        } catch (final FileNotFoundException e) {
            throw new ApplicationException("File not found. Path: " + accessLog);
        } catch (final IOException e) {
            throw new ApplicationException("Something went wrong.", e);
        }
    }
}
