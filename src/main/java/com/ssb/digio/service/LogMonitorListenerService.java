package com.ssb.digio.service;

import com.ssb.digio.entity.LogEntry;
import com.ssb.digio.repository.LogAnalyticsRepository;
import com.ssb.digio.util.ParserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogMonitorListenerService {

    private final LogAnalyticsRepository logAnalyticsRepository;

    @Autowired
    public LogMonitorListenerService(final LogAnalyticsRepository logAnalyticsRepository) {
        this.logAnalyticsRepository = logAnalyticsRepository;
    }

    public void processRecord(final String line) {
        final LogEntry entry = ParserUtil.parseLog(line);
        logAnalyticsRepository.save(entry);
    }
}
