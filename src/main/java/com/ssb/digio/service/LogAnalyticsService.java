package com.ssb.digio.service;

import com.ssb.digio.entity.LogEntry;
import com.ssb.digio.repository.LogAnalyticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class LogAnalyticsService {

    private final LogAnalyticsRepository logAnalyticsRepository;

    @Autowired
    public LogAnalyticsService(final LogAnalyticsRepository logAnalyticsRepository) {
        this.logAnalyticsRepository = logAnalyticsRepository;
    }

    public List<String> getUniqueIpAddresses() {
        return logAnalyticsRepository.findDistinctIpAddresses();
    }

    public List<String> getMostVisitedUrls() {
        return getGroupedData(LogEntry::getUri)
                .entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(Map.Entry.comparingByKey()))
                .limit(3)
                .map(Map.Entry::getKey)
                .toList();
    }

    public List<String> getMostActiveIPs() {
        return getGroupedData(LogEntry::getIpAddress)
                .entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(Map.Entry.comparingByKey()))
                .limit(3)
                .map(Map.Entry::getKey)
                .toList();
    }

    private Map<String, Long> getGroupedData(final Function<LogEntry, String> function) {
        return logAnalyticsRepository.findAll()
                .stream()
                .collect(Collectors.groupingBy(function, Collectors.counting()));
    }
}
