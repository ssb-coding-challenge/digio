package com.ssb.digio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DigIoApplication {

    public static void main(final String[] args) {
        SpringApplication.run(DigIoApplication.class, args);
    }

}
