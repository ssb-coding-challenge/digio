package com.ssb.digio.util;

import com.ssb.digio.entity.LogEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParserUtilTest {

    @Test
    void parseLog() {
        final LogEntry logEntry = ParserUtil.parseLog("50.112.00.11 - admin [11/Jul/2018:17:33:01 +0200] \"GET /asset.css HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\"");

        Assertions.assertEquals("50.112.00.11", logEntry.getIpAddress());
        Assertions.assertEquals("/asset.css", logEntry.getUri());
    }
}