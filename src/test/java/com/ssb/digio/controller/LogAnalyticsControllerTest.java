package com.ssb.digio.controller;

import com.ssb.digio.service.LogAnalyticsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LogAnalyticsController.class)
class LogAnalyticsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LogAnalyticsService service;

    @Test
    void getUniqueIpAddress() throws Exception {
        final List<String> expectedResult = Arrays.asList("10.10.0.8", "10.10.0.20");
        when(service.getUniqueIpAddresses()).thenReturn(expectedResult);

        this.mockMvc.perform(get("/unique-ip"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[\"10.10.0.8\", \"10.10.0.20\"]"));
    }

    @Test
    void getMostVisitedUrls() throws Exception {
        final List<String> expectedResult = Arrays.asList("/", "access.css", "build.js");
        when(service.getMostVisitedUrls()).thenReturn(expectedResult);

        this.mockMvc.perform(get("/most-visited-urls"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[\"/\", \"access.css\", \"build.js\"]"));
    }

    @Test
    void getMostActiveIPs() throws Exception {
        final List<String> expectedResult = Arrays.asList("168.41.191.40", "177.71.128.21", "50.112.00.11");
        when(service.getMostActiveIPs()).thenReturn(expectedResult);

        this.mockMvc.perform(get("/most-active-ips"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().json("[\"168.41.191.40\", \"177.71.128.21\", \"50.112.00.11\"]"));
    }
}