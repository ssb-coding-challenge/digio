package com.ssb.digio.service;

import com.ssb.digio.entity.LogEntry;
import com.ssb.digio.repository.LogAnalyticsRepository;
import org.hamcrest.core.IsIterableContaining;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static java.time.ZonedDateTime.now;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LogAnalyticsServiceTest {

    @Mock
    private LogAnalyticsRepository logAnalyticsRepository;

    @InjectMocks
    private LogAnalyticsService service;

    private static List<LogEntry> logEntryList;

    @BeforeAll
    public static void setup() {
        final LogEntry entry1 = new LogEntry("10.12.20.51", now(), "/");
        final LogEntry entry2 = new LogEntry("10.12.14.53", now(), "/access");
        final LogEntry entry3 = new LogEntry("10.12.20.51", now(), "/login");
        final LogEntry entry4 = new LogEntry("10.12.1.40", now(), "/access");
        final LogEntry entry5 = new LogEntry("10.12.14.33", now(), "/test");
        logEntryList = Arrays.asList(entry1, entry2, entry3, entry4, entry5);
    }

    @Test
    public void testGetUniqueIpAddresses() {
        final List<String> uniIpAddresses = logEntryList.stream().map(LogEntry::getIpAddress).distinct().toList();
        when(logAnalyticsRepository.findDistinctIpAddresses()).thenReturn(uniIpAddresses);
        final List<String> actualResult = service.getUniqueIpAddresses();
        assertThat(actualResult, IsIterableContaining.hasItems("10.12.20.51", "10.12.14.53", "10.12.1.40", "10.12.14.33"));
    }

    @Test
    public void testGetMostVisitedUrls() {
        when(logAnalyticsRepository.findAll()).thenReturn(logEntryList);

        final List<String> actualResult = service.getMostVisitedUrls();
        assertThat(actualResult, containsInAnyOrder("/", "/access", "/login"));
    }

    @Test
    public void testGetMostActiveIPs() {
        when(logAnalyticsRepository.findAll()).thenReturn(logEntryList);

        final List<String> actualResult = service.getMostActiveIPs();
        assertThat(actualResult, containsInAnyOrder("10.12.20.51", "10.12.1.40", "10.12.14.33"));
    }
}