package com.ssb.digio.service;

import com.ssb.digio.entity.LogEntry;
import com.ssb.digio.repository.LogAnalyticsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LogMonitorListenerServiceTest {

    private static final String TEST_RECORD = "50.112.00.11 - admin [11/Jul/2018:17:33:01 +0200] \"GET /asset.css HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\"";

    @Mock
    private LogAnalyticsRepository logAnalyticsRepository;
    @Captor
    ArgumentCaptor<LogEntry> captor;

    @InjectMocks
    private LogMonitorListenerService service;

    @Test
    public void testProcessRecord() {
        service.processRecord(TEST_RECORD);

        verify(logAnalyticsRepository).save(captor.capture());

        final LogEntry value = captor.getValue();
        Assertions.assertEquals("50.112.00.11", value.getIpAddress());
        Assertions.assertEquals("/asset.css", value.getUri());
    }
}