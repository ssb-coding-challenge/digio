package com.ssb.digio.scheduler;

import com.ssb.digio.exception.ApplicationException;
import com.ssb.digio.service.LogMonitorListenerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@ExtendWith(MockitoExtension.class)
class LogMonitorSchedulerTest {

    @Mock
    private LogMonitorListenerService listenerService;
    @Mock
    private Resource resource;
    @Captor
    ArgumentCaptor<String> captor;

    @InjectMocks
    private LogMonitorScheduler scheduler;

    @BeforeEach
    public void beforeEach() {
        setField(scheduler, "schedulerEnabled", true);
        setField(scheduler, "accessLog", resource);
    }

    @Test
    public void readFile() throws IOException {
        when(resource.getFile()).thenReturn(new File("src/test/resources/test-access.log"));
        scheduler.readFile();
        verify(listenerService, times(5)).processRecord(captor.capture());
    }

    @Test
    public void throwErrorWhenFileNotFound() throws IOException {
        when(resource.getFile()).thenReturn(new File("not-found.log"));

        verifyNoInteractions(listenerService);

        final Exception exception = assertThrows(ApplicationException.class, () -> scheduler.readFile());
        assertTrue("com.ssb.digio.exception.ApplicationException: File not found. Path: resource".contains(exception.getMessage()));

    }
}