### Log Analytics Tool

The purpose of this service is to read the logs periodically and store the data in a database. It exposes various
analytics information from the extracted log.

**Note:** Scheduler is configured to every 30 seconds to extract the latest information. This setting can be changed
in *application.yml* or supplied via command line argument.

#### Exposed Endpoints

* The number of unique IP addresses  
  [http://localhost:9595/analytics/unique-ip](http://localhost:9595/analytics/unique-ip)
* The top 3 most visited URLs  
  [http://localhost:9595/analytics/most-visited-urls](http://localhost:9595/analytics/most-visited-urls)
* The top 3 most active IP addresses  
  [http://localhost:9595/analytics/most-active-ips](http://localhost:9595/analytics/most-active-ips)

#### Build and start service

To build the jar  
```mvn clean package```

To start the service  
```java -jar target/DigIO-1.0.0.jar --log.file-path='file:<absolute_path>/<filename>.log'```

#### Database configuration

For test purpose, H2 database has been used and configured to start in server mode.

```
driverClassName: org.h2.Driver
url: jdbc:h2:file:./db;AUTO_SERVER=TRUE
username: sa
password: sa
```

#### Unit test

Unit tests can be executed by running the command   
```mvn clean test```

#### Swagger (OpenAPI)

Swagger UI can be accessed
here [http://localhost:9595/analytics/swagger-ui.html](http://localhost:9595/analytics/swagger-ui.html)